let stack = [];
let done = [];
let newTimer = {};
let newSeconds = 0;
let ready = true;

const engine = () => {
  if (Array.isArray(JSON.parse(localStorage.getItem("done")))) {done = [];
    done = JSON.parse(localStorage.getItem("done"));
  } else {localStorage.setItem("done", JSON.stringify(done));}
  if (done.length > 0) {renderingDone();}};

const renderingDone = () => {
  document.querySelector(".done").innerHTML = "";
  JSON.parse(localStorage.getItem("done")).forEach((e) => {
    document.querySelector(".done").insertAdjacentHTML("beforeend", `<p>Button № ${e.timer} 
    ${new Date(e.click).toLocaleTimeString()} -- ${new Date(e.log).toLocaleTimeString()} (${Math.round(
    (e.log - e.click) / 1000)}   sec )</p>`);});};

const proces = (processed) => {ready = false;
  document.querySelector(".processing").innerHTML = "";
  document.querySelector(".processing").insertAdjacentHTML("afterbegin", `<p>Button# ${processed.timer} 
  ${new Date(processed.click).toLocaleTimeString()} PROCESSING...   </p>`);
  setTimeout(() => {document.querySelector(".processing").innerHTML = "";
    processed.log = new Date().getTime();
    processed.activ = false;
    done.push(processed);
    localStorage.setItem("done", JSON.stringify(done));
    renderingDone();
    ready = true;}, parseInt(processed.timer) * 1000);};

const checker = () => {if (ready && stack.length > 0) {proces(stack.shift());}};

document.querySelectorAll(".seconds").forEach((elem) => {elem.addEventListener("click", (event) => {
    if (event.target.dataset.seconds === "1") {newSeconds = 1;}
    if (event.target.dataset.seconds === "2") {newSeconds = 2;}
    if (event.target.dataset.seconds === "3") {newSeconds = 3;}
    stack.push({ timer: newSeconds, log: 0, click: new Date().getTime(), activ: true,});});});

document.querySelector(".clear").addEventListener("click", () => {done = [];
  localStorage.setItem("done", JSON.stringify(done));
  document.querySelector(".done").innerHTML = "";
  engine();
});

engine();
setInterval(checker, 300);
